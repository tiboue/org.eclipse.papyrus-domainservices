/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.Set;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ActivityExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityFinalNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.ExpansionNode;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.InterruptibleActivityRegion;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link ActivityExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class ActivityExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private ActivityExternalSourceToRepresentationDropBehaviorProvider activityExternalSourceToRepresentationDropBehaviorProvider;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.activityExternalSourceToRepresentationDropBehaviorProvider = new ActivityExternalSourceToRepresentationDropBehaviorProvider();
    }

    @Test
    public void testActivityDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity activityToDrop = this.createIn(Activity.class, root);
        Activity subActivity = this.createIn(Activity.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityToDrop,
                subActivity, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityDropOnActivityPartition() {
        Activity root = this.create(Activity.class);
        Activity activityToDrop = this.createIn(Activity.class, root);
        ActivityPartition partition = this.createIn(ActivityPartition.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityToDrop,
                partition, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testActivityEdgeDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity = this.createIn(Activity.class, root);
        ControlFlow activityEdgeToDrop = this.createIn(ControlFlow.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityEdgeToDrop,
                subActivity, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityEdgeToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityEdgeDropOnActivityGroup() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition = this.createIn(ActivityPartition.class, root);
        ControlFlow activityEdgeToDrop = this.createIn(ControlFlow.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityEdgeToDrop,
                partition, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityEdgeToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityEdgeDropOnActivityNode() {
        Activity root = this.create(Activity.class);
        ActivityFinalNode node = this.createIn(ActivityFinalNode.class, root);
        ControlFlow activityEdgeToDrop = this.createIn(ControlFlow.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityEdgeToDrop,
                node, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityEdgeToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityNodeDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        ActivityFinalNode activityNodeToDrop = this.createIn(ActivityFinalNode.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityNodeToDrop,
                subActivity1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityNodeToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityNodeDropOnActivityGroup() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ActivityFinalNode activityNodeToDrop = this.createIn(ActivityFinalNode.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityNodeToDrop,
                partition1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityNodeToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityNodeDropOnActivityNode() {
        Activity root = this.create(Activity.class);
        ActivityFinalNode node = this.createIn(ActivityFinalNode.class, root);
        ActivityFinalNode activityNodeToDrop = this.createIn(ActivityFinalNode.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityNodeToDrop,
                node, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testActivityParameterNodeDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        ActivityParameterNode activityParameterNodeToDrop = this.createIn(ActivityParameterNode.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider
                .drop(activityParameterNodeToDrop, subActivity1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityParameterNodeToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityParameterNodeDropOnActivityGroup() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ActivityParameterNode activityParameterNodeToDrop = this.createIn(ActivityParameterNode.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider
                .drop(activityParameterNodeToDrop, partition1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testActivityPartitionDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        ActivityPartition activityPartitionToDrop = this.createIn(ActivityPartition.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityPartitionToDrop,
                subActivity1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityPartitionToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityPartitionDropOnActivityPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        ActivityPartition activityPartitionToDrop = this.createIn(ActivityPartition.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityPartitionToDrop,
                partition1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(activityPartitionToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testActivityPartitionDropOnInterruptibleActivityRegion() {
        Activity root = this.create(Activity.class);
        InterruptibleActivityRegion region1 = this.createIn(InterruptibleActivityRegion.class, root);
        ActivityPartition activityPartitionToDrop = this.createIn(ActivityPartition.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(activityPartitionToDrop,
                region1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testCommentDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Comment commentToDrop = this.createIn(Comment.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(commentToDrop,
                subActivity1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(commentToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testCommentDropOnActivityNode() {
        Activity root = this.create(Activity.class);
        ActivityFinalNode node = this.createIn(ActivityFinalNode.class, root);
        Comment commentToDrop = this.createIn(Comment.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(commentToDrop, node,
                this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testCommentDropOnActivityPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        Comment commentToDrop = this.createIn(Comment.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(commentToDrop,
                partition1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(commentToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testConstraintDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        Constraint constraintToDrop = this.createIn(Constraint.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(constraintToDrop,
                subActivity1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(constraintToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testConstraintDropOnActivityPartition() {
        Activity root = this.create(Activity.class);
        ActivityPartition partition1 = this.createIn(ActivityPartition.class, root);
        Constraint constraintToDrop = this.createIn(Constraint.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(constraintToDrop,
                partition1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testConstraintDropOnStructuredActivityNode() {
        Activity root = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode = this.createIn(StructuredActivityNode.class, root);
        Constraint constraintToDrop = this.createIn(Constraint.class, root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(constraintToDrop,
                structuredActivityNode, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(constraintToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testExpansionNodeDropOnActivity() {
        Activity root = this.create(Activity.class);
        ExpansionRegion expansionRegion = this.createIn(ExpansionRegion.class, root);
        ExpansionNode expansionNodeToDrop = this.createIn(ExpansionNode.class, expansionRegion);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(expansionNodeToDrop,
                root, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testExpansionNodeDropOnExpansionRegion() {
        Activity root = this.create(Activity.class);
        ExpansionRegion expansionRegion1 = this.createIn(ExpansionRegion.class, root);
        ExpansionRegion expansionRegion2 = this.createIn(ExpansionRegion.class, root);
        ExpansionNode expansionNodeToDrop = this.createIn(ExpansionNode.class, expansionRegion1);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(expansionNodeToDrop,
                expansionRegion2, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(expansionNodeToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testInterruptibleActivityRegionDropOnActivity() {
        Activity root = this.create(Activity.class);
        Activity subActivity1 = this.createIn(Activity.class, root);
        InterruptibleActivityRegion interruptibleActivityRegionToDrop = this.createIn(InterruptibleActivityRegion.class,
                root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider
                .drop(interruptibleActivityRegionToDrop, subActivity1, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(interruptibleActivityRegionToDrop), status.getElementsToDisplay());
    }

    @Test
    public void testInterruptibleActivityRegionDropOnInterruptibleActivityRegion() {
        Activity root = this.create(Activity.class);
        InterruptibleActivityRegion interruptibleActivityRegion = this.createIn(InterruptibleActivityRegion.class,
                root);
        InterruptibleActivityRegion interruptibleActivityRegionToDrop = this.createIn(InterruptibleActivityRegion.class,
                root);

        DnDStatus status = this.activityExternalSourceToRepresentationDropBehaviorProvider.drop(
                interruptibleActivityRegionToDrop, interruptibleActivityRegion, this.getCrossRef(),
                this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }
}
