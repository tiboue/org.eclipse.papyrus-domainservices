/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.properties.mock.MockEditableChecker;
import org.eclipse.papyrus.uml.domain.services.properties.mock.MockLogger;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link PropertiesValueSpecificationServices} service class.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PropertiesValueSpecificationServicesTest extends AbstractPropertiesServicesTest {

	/**
	 * Wildcard string.
	 */
	private static final String STAR = "*"; //$NON-NLS-1$

	/**
	 * An arbitrary string to initialize tests with a wrong value.
	 */
	private static final String A_STRING = "a"; //$NON-NLS-1$

	/**
	 * Integer as String.
	 */
	private static final String INTEGER_AS_STRING = "15"; //$NON-NLS-1$

	/**
	 * Double as String.
	 */
	private static final String DOUBLE_AS_STRING = "1.0"; //$NON-NLS-1$

	/**
	 * The instance of PropertiesServices being tested.
	 */
	private PropertiesValueSpecificationServices propertiesService;

    @BeforeEach
	public void setUp() {
        this.propertiesService = new PropertiesValueSpecificationServices(new MockLogger(), new MockEditableChecker());
	}

	@Test
	public void testGetLiteralIntegerValue() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
		int value = 1;
		literalInteger.setValue(value);
        assertEquals(Integer.toString(value), propertiesService.getLiteralIntegerValue(literalInteger));
	}

	@Test
	public void testGetLiteralRealValue() {
		LiteralReal literalReal = create(LiteralReal.class);
		double value = 1.0;
		literalReal.setValue(value);
        assertEquals(Double.toString(value), propertiesService.getLiteralRealValue(literalReal));
	}

	@Test
	public void testGetLiteralUnlimitedNaturalValue() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
		int value = 1;
		literalUnlimitedNatural.setValue(value);
        assertEquals(Integer.toString(value),
                propertiesService.getLiteralUnlimitedNaturalValue(literalUnlimitedNatural));
		value = -1;
		literalUnlimitedNatural.setValue(value);
        assertEquals(STAR, propertiesService.getLiteralUnlimitedNaturalValue(literalUnlimitedNatural));
	}

	@Test
	public void testGetDefaultLiteralIntegerValue() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
		int value = 0;
        assertEquals(Integer.toString(value), propertiesService.getLiteralIntegerValue(literalInteger));
	}

	@Test
	public void testGetDefaultLiteralRealValue() {
		LiteralReal literalReal = create(LiteralReal.class);
		double value = 0.0;
        assertEquals(Double.toString(value), propertiesService.getLiteralRealValue(literalReal));
	}

	@Test
	public void testGetDefaultLiteralUnlimitedNaturalValue() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
		int value = 0;
        assertEquals(Integer.toString(value),
                propertiesService.getLiteralUnlimitedNaturalValue(literalUnlimitedNatural));
	}

	@Test
	public void testSetLiteralIntegerValue() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
		literalInteger.setValue(1);
        assertTrue(propertiesService.setLiteralIntegerValue(literalInteger, Integer.valueOf(2)));
        assertEquals(2, literalInteger.getValue());
	}

	@Test
	public void testSetLiteralIntegerValueWithStringInteger() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
        assertTrue(propertiesService.setLiteralIntegerValue(literalInteger, INTEGER_AS_STRING));
		assertEquals(15, literalInteger.getValue());
	}

	@Test
	public void testSetLiteralIntegerValueWithInteger() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
        assertTrue(propertiesService.setLiteralIntegerValue(literalInteger, Integer.valueOf(1)));
		assertEquals(1, literalInteger.getValue());
	}

	@Test
	public void testSetLiteralIntegerValueWithString() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
        assertFalse(propertiesService.setLiteralIntegerValue(literalInteger, A_STRING));
		assertEquals(0, literalInteger.getValue());
	}

	@Test
	public void testSetLiteralIntegerValueWithNull() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
        assertFalse(propertiesService.setLiteralIntegerValue(literalInteger, null));
		assertEquals(0, literalInteger.getValue());
	}

	@Test
	public void testSetLiteralRealValue() {
		LiteralReal literalReal = create(LiteralReal.class);
		literalReal.setValue(1.0);
        assertTrue(propertiesService.setLiteralRealValue(literalReal, Double.valueOf(2.0)));
        assertEquals(2.0, literalReal.getValue(), 0);
	}

	@Test
	public void testSetLiteralRealValueWithStringDouble() {
		LiteralReal literalReal = create(LiteralReal.class);
        assertTrue(propertiesService.setLiteralRealValue(literalReal, DOUBLE_AS_STRING));
		assertEquals(1, literalReal.getValue(), 0);
	}

	@Test
	public void testSetLiteralRealValueWithDouble() {
		LiteralReal literalReal = create(LiteralReal.class);
        assertTrue(propertiesService.setLiteralRealValue(literalReal, Double.valueOf(1.0)));
		assertEquals(1, literalReal.getValue(), 0);
	}

	@Test
	public void testSetLiteralRealValueWithString() {
		LiteralReal literalReal = create(LiteralReal.class);
        assertFalse(propertiesService.setLiteralRealValue(literalReal, A_STRING));
		assertEquals(0, literalReal.getValue(), 0);
	}

	@Test
	public void testSetLiteralRealValueWithNull() {
		LiteralReal literalReal = create(LiteralReal.class);
        assertFalse(propertiesService.setLiteralRealValue(literalReal, null));
		assertEquals(0, literalReal.getValue(), 0);
	}

	@Test
	public void testSetLiteralUnlimitedNaturalValue() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
		literalUnlimitedNatural.setValue(1);
        assertTrue(propertiesService.setLiteralUnlimitedNaturalValue(literalUnlimitedNatural, Integer.valueOf(2)));
        assertEquals(2, literalUnlimitedNatural.getValue());
	}

	@Test
	public void testSetLiteralUnlimitedNaturalValueWithIntegerValue() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
        assertTrue(propertiesService.setLiteralUnlimitedNaturalValue(literalUnlimitedNatural, INTEGER_AS_STRING));
		assertEquals(15, literalUnlimitedNatural.getValue());
        assertTrue(propertiesService.setLiteralUnlimitedNaturalValue(literalUnlimitedNatural, STAR));
		assertEquals(-1, literalUnlimitedNatural.getValue());
	}

	@Test
	public void testSetLiteralUnlimitedNaturalValueWithInteger() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
        assertTrue(propertiesService.setLiteralUnlimitedNaturalValue(literalUnlimitedNatural, Integer.valueOf(1)));
		assertEquals(1, literalUnlimitedNatural.getValue());
	}

	@Test
	public void testSetLiteralUnlimitedNaturalValueWithString() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
        assertFalse(propertiesService.setLiteralUnlimitedNaturalValue(literalUnlimitedNatural, A_STRING));
		assertEquals(0, literalUnlimitedNatural.getValue());
	}

	@Test
	public void testSetLiteralUnlimitedNaturalValueWithNull() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
        assertFalse(propertiesService.setLiteralUnlimitedNaturalValue(literalUnlimitedNatural, null));
		assertEquals(0, literalUnlimitedNatural.getValue());
	}

	@Test
	public void testValidateLiteralIntegerField() {
		LiteralInteger literalInteger = create(LiteralInteger.class);
		literalInteger.setValue(1);
        assertTrue(propertiesService.validateLiteralIntegerField(literalInteger));
	}

	@Test
	public void testValidateLiteralRealField() {
		LiteralReal literalReal = create(LiteralReal.class);
		literalReal.setValue(1.0);
        assertTrue(propertiesService.validateLiteralRealField(literalReal));
	}

	@Test
	public void testValidateLiteralUnlimitedNaturalField() {
		LiteralUnlimitedNatural literalUnlimitedNatural = create(LiteralUnlimitedNatural.class);
		literalUnlimitedNatural.setValue(1);
        assertTrue(propertiesService.validateLiteralUnlimitedNaturalField(literalUnlimitedNatural));
	}
}
