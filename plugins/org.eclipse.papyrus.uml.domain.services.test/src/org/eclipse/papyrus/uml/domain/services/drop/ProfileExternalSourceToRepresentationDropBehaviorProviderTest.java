/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ProfileExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for
 * {@link ProfileExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal DANIEL</a>
 */
public class ProfileExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private ProfileExternalSourceToRepresentationDropBehaviorProvider useCaseExternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.useCaseExternalSourceToRepresentationDropBehaviorProvider = new ProfileExternalSourceToRepresentationDropBehaviorProvider();
    }

    /**
     * Test dropping an element into another.
     *
     * @param classToDrop
     *                        the type of the element to drop
     * @param containingClass
     *                        the type of the element containing the element to drop
     * @param expectedResult
     *                        true if NOTHING is expected, false if FAILED is
     *                        expected.
     *
     */
    @ParameterizedTest
    @MethodSource("getDragAndDropParameters")
    public void testDragAndDrop(Class<? extends Element> classToDrop, Class<? extends Element> containingClass,
            boolean expectedResult) {
        Element elementToDrop = this.create(classToDrop);
        Element containingElement = this.create(containingClass);

        DnDStatus status = this.useCaseExternalSourceToRepresentationDropBehaviorProvider.drop(elementToDrop,
                containingElement, this.getCrossRef(), this.getEditableChecker());
        State expectedState = State.FAILED;
        if (expectedResult) {
            assertTrue(List.of(elementToDrop).containsAll(status.getElementsToDisplay()));
            expectedState = State.NOTHING;
        }
        assertEquals(expectedState, status.getState());
    }

    public static Stream<Arguments> getDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                Arguments.of(org.eclipse.uml2.uml.Class.class, Package.class, true),
                Arguments.of(org.eclipse.uml2.uml.Class.class, Profile.class, true),
                Arguments.of(org.eclipse.uml2.uml.Class.class, Comment.class, false),
                Arguments.of(Comment.class, Package.class, true),
                Arguments.of(Comment.class, Profile.class, true),
                Arguments.of(Comment.class, Comment.class, false),
                Arguments.of(Constraint.class, Package.class, true),
                Arguments.of(Constraint.class, Profile.class, true),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(DataType.class, Package.class, true),
                Arguments.of(DataType.class, Profile.class, true),
                Arguments.of(DataType.class, Comment.class, false),
                Arguments.of(Enumeration.class, Package.class, true),
                Arguments.of(Enumeration.class, Profile.class, true),
                Arguments.of(Enumeration.class, Comment.class, false),
                Arguments.of(EnumerationLiteral.class, Enumeration.class, true),
                Arguments.of(EnumerationLiteral.class, Comment.class, false),
                Arguments.of(ElementImport.class, Profile.class, true),
                Arguments.of(ElementImport.class, Comment.class, false),
                Arguments.of(Operation.class, org.eclipse.uml2.uml.Class.class, true),
                Arguments.of(Operation.class, DataType.class, true),
                Arguments.of(Operation.class, Stereotype.class, true),
                Arguments.of(Operation.class, Enumeration.class, false),
                Arguments.of(Operation.class, Comment.class, false),
                Arguments.of(Package.class, Package.class, true),
                Arguments.of(Package.class, Profile.class, true),
                Arguments.of(Package.class, Comment.class, false),
                Arguments.of(PrimitiveType.class, Package.class, true),
                Arguments.of(PrimitiveType.class, Profile.class, true),
                Arguments.of(PrimitiveType.class, Comment.class, false),
                Arguments.of(Profile.class, Package.class, true),
                Arguments.of(Profile.class, Profile.class, true),
                Arguments.of(Profile.class, Comment.class, false),
                Arguments.of(Property.class, Class.class, true),
                Arguments.of(Property.class, DataType.class, true),
                Arguments.of(Property.class, Enumeration.class, false),
                Arguments.of(Property.class,  Stereotype.class, true),
                Arguments.of(Property.class, Comment.class, false),
                Arguments.of(Stereotype.class, Package.class, true),
                Arguments.of(Stereotype.class, Profile.class, true),
                Arguments.of(Stereotype.class, Comment.class, false),
                // Relationships can be dropped anywhere on the diagram.
                Arguments.of(Extension.class, Package.class, true),
                Arguments.of(Extension.class, Comment.class, true),
                Arguments.of(Artifact.class, Package.class, false)
                );
        // @formatter:on
    }

}
