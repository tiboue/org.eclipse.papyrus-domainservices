/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.papyrus.uml.domain.services.create.ElementConfigurer;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.DecisionNode;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentTarget;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ForkNode;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeReconnectTargetBehaviorProvider}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class ElementDomainBasedEdgeReconnectTargetBehaviorProviderTest extends AbstractUMLTest {

    /* Name class2 */
    private static final String CLASS2 = "class2";

    /* Name class1 */
    private static final String CLASS1 = "class1";

    private static final String STEREOTYPE = "stereotype";

    /**
     * Test reconnect target of association on an other target (target property is
     * not owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyNotOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classSource = this.createIn(Class.class, model);
        Class classOldTarget = this.createIn(Class.class, model);
        classOldTarget.setName(CLASS1);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);

        // change owner of target Property
        Property targetProperty = association.getMemberEnds().get(1);
        classOldTarget.getOwnedAttributes().add(targetProperty);
        Class classNewTarget = this.createIn(Class.class, model);
        classNewTarget.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(association, classOldTarget, classNewTarget, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(1, association.getOwnedMembers().size());
        assertFalse(classOldTarget.getOwnedAttributes().contains(targetProperty));
        assertTrue(classNewTarget.getOwnedAttributes().contains(targetProperty));
        Property sourceProperty = association.getMemberEnds().get(0);
        assertEquals(classNewTarget, sourceProperty.getType());
        assertEquals(classOldTarget.getName(), sourceProperty.getName());
    }

    /**
     * Test reconnect target of association on an other target (target property is
     * owned by the association).
     */
    @Test
    public void testAssociationSourceReconnectWithPropertyOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classSource = this.createIn(Class.class, model);
        Class classOldTarget = this.createIn(Class.class, model);
        classOldTarget.setName(CLASS1);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);
        Class classNewTarget = this.createIn(Class.class, model);
        classNewTarget.setName(CLASS2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(association, classOldTarget, classNewTarget, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(2, association.getOwnedMembers().size());
        Property targetProperty = association.getMemberEnds().get(1);
        assertEquals(classSource, targetProperty.getType());
        Property sourceProperty = association.getMemberEnds().get(0);
        assertEquals(classNewTarget, sourceProperty.getType());
        assertEquals(classOldTarget.getName(), sourceProperty.getName());
    }

    /**
     * Test reconnect target of association on an other target in different actor.
     * Expected result : target changed
     */
    @Test
    public void testAssociationTargetReconnect() {
        Actor source = this.create(Actor.class);
        Property sourceProp = this.create(Property.class);
        sourceProp.setType(source);

        Actor oldTarget = this.create(Actor.class);
        Property targetProp = this.create(Property.class);
        targetProp.setType(oldTarget);

        Actor newTarget = this.create(Actor.class);

        Association association = this.create(Association.class);
        association.getMemberEnds().add(targetProp);
        association.getMemberEnds().add(sourceProp);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(association, oldTarget, newTarget, null);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals(source, association.getMemberEnds().get(1).getType());
        assertEquals(newTarget, association.getMemberEnds().get(0).getType());

    }

    /**
     * Default reconnection for {@link ComponentRealization}.
     */
    @Test
    public void testComponentRealizationReconnect() {
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        ComponentRealization componentRealization = this.createIn(ComponentRealization.class, targetComponent);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);
        Component newTargetComponent = this.create(Component.class);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(componentRealization, targetComponent, newTargetComponent, null);

        assertTrue(status.isValid());
        // Make sure the provider didn't produce unexpected side effects on the
        // component realization source.
        assertEquals(List.of(sourceInterface), componentRealization.getRealizingClassifiers());
        assertEquals(List.of(sourceInterface), componentRealization.getClients());
        assertEquals(newTargetComponent, componentRealization.getAbstraction());
        assertEquals(List.of(newTargetComponent), componentRealization.getSuppliers());
        // ComponentRealization are contained in their target, so reconnecting the
        // target should update the container.
        assertEquals(newTargetComponent, componentRealization.eContainer());
    }

    /**
     * Test the target reconnection for a connector on a classifier port. This test
     * aims to check whether the partWithPort attribute is set at null.
     */
    @Test
    public void testConnectorTargetReconnectOnClassifierPort() {
        // Create semantic elements
        Class class1 = this.create(Class.class);
        Class class2 = this.create(Class.class);
        Port oldTargetPort = this.createIn(Port.class, class1);
        Port newTargetPort = this.createIn(Port.class, class2);
        Port sourcePort = this.createIn(Port.class, class1);
        Property property = this.create(Property.class);
        property.setType(class1);
        Connector connector = this.create(Connector.class);
        ConnectorEnd connectorEndSource = this.createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(sourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = this.createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(oldTargetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode class2Node = visualTree.addChildren(class2);
        VisualNode visualNewTargetPort = class2Node.addChildren(newTargetPort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(connector, oldTargetPort, newTargetPort, visualNewTargetPort);
        assertTrue(status.isValid());
        assertEquals(newTargetPort, connectorEndTarget.getRole(),
                "The new source port should have changed after the reconnect.");
        assertNull(connectorEndTarget.getPartWithPort(), "The old part with port should have been removed");
    }

    /**
     * Test the target reconnection for a connector on a property port.
     */
    @Test
    public void testConnectorTargetReconnectOnPropertyPort() {
        // Create semantic elements
        Class class1 = this.create(Class.class);
        Port oldTargetPort = this.createIn(Port.class, class1);
        Port newTargetPort = this.createIn(Port.class, class1);
        Port sourcePort = this.createIn(Port.class, class1);
        Property property = this.create(Property.class);
        property.setType(class1);
        Connector connector = this.create(Connector.class);
        ConnectorEnd connectorEndSource = this.createIn(ConnectorEnd.class, connector);
        connectorEndSource.setRole(sourcePort);
        connectorEndSource.setPartWithPort(property);
        ConnectorEnd connectorEndTarget = this.createIn(ConnectorEnd.class, connector);
        connectorEndTarget.setRole(oldTargetPort);
        connectorEndTarget.setPartWithPort(property);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode propertyNode = visualTree.addChildren(property);
        VisualNode visualNewTargetPort = propertyNode.addChildren(newTargetPort);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(connector, oldTargetPort, newTargetPort, visualNewTargetPort);
        assertTrue(status.isValid());
        assertEquals(newTargetPort, connectorEndTarget.getRole(),
                "The new source port should have changed after the reconnect.");
        assertEquals(property, connectorEndTarget.getPartWithPort());
    }

    /**
     * Check the ControlFlow reconnect.
     */
    @Test
    public void testControlFlowTargetReconnect() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeTargetReconnect(controlFlow);
    }

    /**
     * Tests that the ControlFlow reconnect updates owner feature when the new
     * target is in a different {@link StructuredActivityNode}.
     */
    @Test
    public void testControlFlowTargetReconnectDifferentStructuredActivityNode() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeTargetReconnectDifferentStructuredActivityNode(controlFlow);
    }

    /**
     * Tests that the ControlFlow reconnect updates owner feature when the new
     * target is in the same {@link StructuredActivityNode} (and the previous target
     * was in a different {@link StructuredActivityNode}).
     */
    @Test
    public void testControlFlowTargetReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeTargetReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode(
                controlFlow);
    }

    /**
     * Test that the ControlFlow reconnect also updates the InPartition feature.
     */
    @Test
    public void testControlFlowTargetReconnectInPartition() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        this.testActivityEdgeTargetReconnectInPartition(controlFlow);
    }

    /**
     * Test reconnect target of dependency on an other target in different package.
     * Expected result : target changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencyTargetReconnectInDifferentPackage() {
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = this.createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        org.eclipse.uml2.uml.Package p2 = this.create(org.eclipse.uml2.uml.Package.class);
        org.eclipse.uml2.uml.Class c3 = this.createIn(org.eclipse.uml2.uml.Class.class, p2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, c2, c3, null);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getSuppliers().size());
        assertEquals(c3, dependency.getSuppliers().get(0));
    }

    /**
     * Test reconnect target of dependency on an other target in the same package.
     * Expected result : target changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencyTargetReconnectInSamePackage() {
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = this.createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        org.eclipse.uml2.uml.Class c3 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, c2, c3, null);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getSuppliers().size());
        assertEquals(c3, dependency.getSuppliers().get(0));
    }

    /**
     * Test reconnect target with null dependency or null old target or null new
     * target.
     */
    @Test
    public void testDependencyTargetReconnectWithNullArguments() {
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = this.createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = this.createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(null, c1, c2, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, c1, null, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(dependency, null, c1, null);

        assertFalse(status.isValid());

    }

    /**
     * Test reconnect target of {@link Deployment} on another
     * {@link DeploymentTarget} target.
     */
    @Test
    public void testDeploymentTargetReconnect() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact = this.create(Artifact.class);
        Node node1 = this.create(Node.class);
        Node node2 = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact);
        deployment.setLocation(node1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(deployment, node1, node2, null);
        assertTrue(status.isValid());
        assertEquals(node2, deployment.getLocation());
    }

    /**
     * Test reconnect target of {@link Extend} on an other {@link UseCase} source.
     */
    @Test
    public void testExtendTargetReconnect() {
        UseCase target = this.create(UseCase.class);
        UseCase newTarget = this.create(UseCase.class);

        Extend extend = this.create(Extend.class);
        extend.setExtendedCase(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(extend, target, newTarget, null);

        assertTrue(status.isValid());
        assertEquals(newTarget, extend.getExtendedCase());
    }

    /**
     * Test reconnect {@link Extension} target.
     */
    @Test
    public void testExtensionTargetReconnect() {
        Profile profile = this.create(Profile.class);
        Stereotype stereotype = this.create(Stereotype.class);
        stereotype.setName(STEREOTYPE);
        Class class1 = this.create(Class.class);
        class1.setName(CLASS1);
        Class class2 = this.create(Class.class);
        class2.setName(CLASS2);
        Extension extension = this.createIn(Extension.class, profile);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(extension, extension.eContainer());
        assertEquals("E_stereotype_class11", extension.getName());
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(extension, class1, class2, null);
        assertTrue(status.isValid());
        assertEquals(class2, extension.getMetaclass());
        assertEquals("E_stereotype_class21", extension.getName());

        // now check that the name is unchanged if the user has customized it.
        extension.setName("customName");
        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(extension, class2, class1, null);
        assertTrue(status.isValid());
        assertEquals(class1, extension.getMetaclass());
        assertEquals("customName", extension.getName());
    }

    @Test
    public void testGeneralizationTargetReconnect() {
        UseCase target = this.create(UseCase.class);
        UseCase target2 = this.create(UseCase.class);

        Generalization generalization = this.create(Generalization.class);
        generalization.setGeneral(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(generalization, target, target2, null);

        assertTrue(status.isValid());
        assertEquals(target2, generalization.getGeneral());
    }

    /**
     * Default target reconnection for {@link Include}.
     */
    @Test
    public void testIncludeTargetReconnect() {

        UseCase target = this.create(UseCase.class);
        UseCase target2 = this.create(UseCase.class);

        Include include = this.create(Include.class);
        include.setAddition(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(include, target, target2, null);

        assertTrue(status.isValid());
        assertEquals(target2, include.getAddition());
    }

    /**
     * Default target reconnection for {@link InformationFlow}.
     */
    @Test
    public void testInformationFlowTargetReconnect() {
        Package package1 = this.create(Package.class);
        Class target = this.createIn(Class.class, package1);
        Class source = this.createIn(Class.class, package1);
        Class newTarget = this.createIn(Class.class, package1);
        InformationFlow informationFlow = this.createIn(InformationFlow.class, package1);
        new ElementDomainBasedEdgeInitializer().initialize(informationFlow, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(informationFlow, target, newTarget, null);
        assertTrue(status.isValid());
        assertTrue(informationFlow.getInformationTargets().size() == 1);
        assertTrue(informationFlow.getInformationTargets().contains(newTarget));
    }

    /**
     * Default reconnection for {@link InterfaceRealization}.
     */
    @Test
    public void testInterfaceRealizationReconnect() {

        Class classSource = this.create(Class.class);
        Interface interfaceTarget = this.create(Interface.class);
        Interface interfaceTarget2 = this.create(Interface.class);

        InterfaceRealization interfaceRealization = this.createIn(InterfaceRealization.class, classSource);
        interfaceRealization.setImplementingClassifier(classSource);
        interfaceRealization.setContract(interfaceTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(interfaceRealization, interfaceTarget, interfaceTarget2, null);
        assertTrue(status.isValid());
        assertEquals(interfaceTarget2, interfaceRealization.getContract());
    }

    /**
     * Default target reconnection for {@link Manifestation}.
     */
    @Test
    public void testManifestationTargetReconnect() {
        Class target = this.create(Class.class);
        Class source = this.create(Class.class);
        Class newTarget = this.create(Class.class);
        Manifestation manifestation = this.create(Manifestation.class);
        new ElementDomainBasedEdgeInitializer().initialize(manifestation, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(manifestation, target, newTarget, null);
        assertTrue(status.isValid());
        assertTrue(manifestation.getUtilizedElement().equals(newTarget));
    }

    /**
     * Default target reconnection for {@link Message}.
     */
    @Test
    public void testMessageTargetReconnect() {
        Lifeline target = this.create(Lifeline.class);
        Lifeline source = this.create(Lifeline.class);
        Lifeline newTarget = this.create(Lifeline.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification receiveEvent = UMLFactory.eINSTANCE.createMessageOccurrenceSpecification();
        receiveEvent.setMessage(message);
        message.setReceiveEvent(receiveEvent);
        new ElementDomainBasedEdgeInitializer().initialize(message, source, target, null, null, null);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(message, target, newTarget, null);
        assertTrue(status.isValid());
        assertTrue(((MessageOccurrenceSpecification) message.getReceiveEvent()).getCovereds().get(0).equals(newTarget));
    }

    /**
     * Check the ObjectFlow reconnect.
     */
    @Test
    public void testObjectFlowTargetReconnect() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeTargetReconnect(objectFlow);
    }

    /**
     * Tests that the ObjectFlow reconnect updates owner feature when the new target
     * is in a different {@link StructuredActivityNode}.
     */
    @Test
    public void testObjectFlowTargetReconnectDifferentStructuredActivityNode() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeTargetReconnectDifferentStructuredActivityNode(objectFlow);
    }

    /**
     * Tests that the ObjectFlow reconnect updates owner feature when the new target
     * is in the same {@link StructuredActivityNode} (and the previous target was in
     * a different {@link StructuredActivityNode}).
     */
    @Test
    public void testObjectFlowTargetReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeTargetReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode(objectFlow);
    }

    /**
     * Test that the ObjectFlow reconnect also updates the InPartition feature.
     */
    @Test
    public void testObjectFlowTargetReconnectInPartition() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        this.testActivityEdgeTargetReconnectInPartition(objectFlow);
    }

    /**
     * Default reconnection for {@link PackageImport}.
     */
    @Test
    public void testPackageImportTargetReconnect() {

        Package packSource = this.create(Package.class);
        Package packTarget = this.create(Package.class);
        Package packTarget2 = this.create(Package.class);

        PackageImport packImport = this.createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(packImport, packTarget, packTarget2, null);
        assertTrue(status.isValid());
        assertEquals(packTarget2, packImport.getImportedPackage());
    }

    /**
     * Default reconnection for {@link PackageMerge}.
     */
    @Test
    public void testPackageMergeTargetReconnect() {

        Package packSource = this.create(Package.class);
        Package packTarget = this.create(Package.class);
        Package packTarget2 = this.create(Package.class);

        PackageMerge merge = this.createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(merge, packTarget, packTarget2, null);
        assertTrue(status.isValid());
        assertEquals(packTarget2, merge.getMergedPackage());
    }

    /**
     * Default target reconnection for {@link Substitution}.
     */
    @Test
    public void testSubstitutionTargetReconnect() {
        Class clazzOldTarget = this.create(Class.class);
        Class clazzNewTarget = this.create(Class.class);
        Class clazzSource = this.create(Class.class);
        Substitution substitution = this.create(Substitution.class);
        clazzSource.getSubstitutions().add(substitution);
        new ElementDomainBasedEdgeInitializer().initialize(substitution, clazzSource, clazzOldTarget, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(substitution, clazzOldTarget, clazzNewTarget, null);

        assertTrue(status.isValid());
        assertEquals(clazzNewTarget, substitution.getContract());
    }

    /**
     * Default target reconnection for {@link Transition}.
     */
    @Test
    public void testTransitionTargetReconnect() {
        State state1 = this.create(State.class);
        Pseudostate source = this.createIn(Pseudostate.class, state1);
        Region region = this.createIn(Region.class, state1);
        Pseudostate target1 = this.createIn(Pseudostate.class, region);
        Pseudostate target2 = this.createIn(Pseudostate.class, region);

        Transition transition = this.createIn(Transition.class, region);
        transition.setSource(source);
        transition.setTarget(target1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(transition, target1, target2, null);

        assertTrue(status.isValid());
        assertEquals(region, transition.getOwner());
        assertEquals(source, transition.getSource());
        assertEquals(target2, transition.getTarget());
    }

    private void testActivityEdgeTargetReconnect(ActivityEdge activityEdge) {
        ForkNode forkNode = this.create(ForkNode.class);
        DecisionNode decisionNode = this.create(DecisionNode.class);
        JoinNode joinNode = this.create(JoinNode.class);
        activityEdge.setSource(forkNode);
        activityEdge.setTarget(joinNode);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(activityEdge, joinNode, decisionNode, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode, activityEdge.getTarget());
    }

    private void testActivityEdgeTargetReconnectDifferentStructuredActivityNode(ActivityEdge activityEdge) {
        Activity rootActivity = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode1 = this.createIn(StructuredActivityNode.class, rootActivity);
        StructuredActivityNode structuredActivityNode2 = this.createIn(StructuredActivityNode.class, rootActivity);
        DecisionNode decisionNode1 = this.createIn(DecisionNode.class, structuredActivityNode1);
        DecisionNode decisionNode2 = this.createIn(DecisionNode.class, structuredActivityNode1);
        DecisionNode decisionNode3 = this.createIn(DecisionNode.class, structuredActivityNode2);
        ElementDomainBasedEdgeInitializer initializer = new ElementDomainBasedEdgeInitializer();
        initializer.initialize(activityEdge, decisionNode1, decisionNode2, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(activityEdge, decisionNode2, decisionNode3, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode3, activityEdge.getTarget());
        assertEquals(rootActivity, activityEdge.getOwner());
    }

    private void testActivityEdgeTargetReconnectFromDifferentStructuredActivityNodeToSameStructuredActivityNode(
            ActivityEdge activityEdge) {
        Activity rootActivity = this.create(Activity.class);
        StructuredActivityNode structuredActivityNode1 = this.createIn(StructuredActivityNode.class, rootActivity);
        StructuredActivityNode structuredActivityNode2 = this.createIn(StructuredActivityNode.class, rootActivity);
        DecisionNode decisionNode1 = this.createIn(DecisionNode.class, structuredActivityNode1);
        DecisionNode decisionNode2 = this.createIn(DecisionNode.class, structuredActivityNode1);
        DecisionNode decisionNode3 = this.createIn(DecisionNode.class, structuredActivityNode2);
        ElementDomainBasedEdgeInitializer initializer = new ElementDomainBasedEdgeInitializer();
        initializer.initialize(activityEdge, decisionNode1, decisionNode3, null, null, null);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(activityEdge, decisionNode3, decisionNode2, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode2, activityEdge.getTarget());
        assertEquals(structuredActivityNode1, activityEdge.getOwner());
    }

    private void testActivityEdgeTargetReconnectInPartition(ActivityEdge activityEdge) {
        Activity activity = this.create(Activity.class);
        ActivityPartition activityPartition = this.createIn(ActivityPartition.class, activity);
        ActivityPartition activityPartition2 = this.createIn(ActivityPartition.class, activity);
        ForkNode forkNode = this.createIn(ForkNode.class, activity);
        forkNode.getInPartitions().addAll(List.of(activityPartition, activityPartition2));
        JoinNode joinNode = this.createIn(JoinNode.class, activity);
        joinNode.getInPartitions().add(activityPartition);
        activityEdge.setSource(forkNode);
        activityEdge.setTarget(joinNode);
        activityEdge.getInPartitions().add(activityPartition);

        // We try to reconnect on a source out of the activityPartition.
        DecisionNode decisionNode = this.create(DecisionNode.class);
        decisionNode.getInPartitions().add(activityPartition2);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider(new MockedViewQuerier())
                .reconnectTarget(activityEdge, joinNode, decisionNode, null);
        assertTrue(status.isValid());
        assertEquals(decisionNode, activityEdge.getTarget());
        assertEquals(1, activityEdge.getInPartitions().size());
        assertEquals(activityPartition2, activityEdge.getInPartitions().get(0));
    }
}
