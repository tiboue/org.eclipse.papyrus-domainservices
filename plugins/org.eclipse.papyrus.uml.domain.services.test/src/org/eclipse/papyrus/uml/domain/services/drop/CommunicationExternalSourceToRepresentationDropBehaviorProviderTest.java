/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CommunicationExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TimeObservation;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link CommunicationExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class CommunicationExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Test dropping a {@link Activity} on {@link DurationObservation}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testActivityDropOnDurationObservation() {
        DurationObservation durationObservation = this.create(DurationObservation.class);
        Activity activity = this.create(Activity.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(activity,
                durationObservation, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
    }

    /**
     * Test dropping a {@link Activity} on {@link Interaction}.
     *
     * Expected result : new Property typed with the Activity, new Lifeline with
     * Lifeline.represents = Property and Lifeline will be represented graphically.
     */
    @Test
    public void testActivityDropOnInteraction() {
        Interaction interaction = this.create(Interaction.class);
        Activity activity = this.createIn(Activity.class, interaction);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(activity,
                interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        EList<Property> ownedAttributes = interaction.getOwnedAttributes();
        assertNotNull(ownedAttributes);
        assertEquals(1, ownedAttributes.size());
        Property property = ownedAttributes.get(0);
        assertEquals(activity, property.getType());
        EList<Lifeline> lifelines = interaction.getLifelines();
        assertEquals(1, lifelines.size());
        Lifeline lifeline = lifelines.get(0);
        assertEquals(property, lifeline.getRepresents());
        assertEquals(Set.of(lifeline), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Activity} on {@link Lifeline}.
     *
     * Expected result : new Property typed with the Activity and
     * Lifeline.represents = Property.
     */
    @Test
    public void testActivityDropOnLifeline() {
        Interaction interaction = this.create(Interaction.class);
        Activity activity = this.createIn(Activity.class, interaction);
        Lifeline lifeline = this.createIn(Lifeline.class, interaction);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(activity,
                lifeline, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        EList<Property> ownedAttributes = interaction.getOwnedAttributes();
        assertNotNull(ownedAttributes);
        assertEquals(1, ownedAttributes.size());
        Property property = ownedAttributes.get(0);
        assertEquals(activity, property.getType());
        assertEquals(property, lifeline.getRepresents());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Comment} on {@link Interaction}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testCommentDropOnInteraction() {
        Comment comment = this.create(Comment.class);
        Interaction interaction = this.create(Interaction.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(comment,
                interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(comment), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Comment} on {@link Lifeline}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testCommentDropOnLifeline() {
        Comment comment = this.create(Comment.class);
        Lifeline lifeline = this.create(Lifeline.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(comment, lifeline,
                this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Interaction}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testConstraintDropOnInteraction() {
        Constraint constraint = this.create(Constraint.class);
        Interaction interaction = this.create(Interaction.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(constraint,
                interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(constraint), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Lifeline}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testConstraintDropOnLifeline() {
        Constraint constraint = this.create(Constraint.class);
        Lifeline lifeline = this.create(Lifeline.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(constraint,
                lifeline, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link DurationObservation} on {@link Interaction}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testDurationObservationDropOnInteraction() {
        DurationObservation durationObservation = this.create(DurationObservation.class);
        Interaction interaction = this.create(Interaction.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider()
                .drop(durationObservation, interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(durationObservation), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link DurationObservation} on {@link Lifeline}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testDurationObservationDropOnLifeline() {
        DurationObservation durationObservation = this.create(DurationObservation.class);
        Lifeline lifeline = this.create(Lifeline.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider()
                .drop(durationObservation, lifeline, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Lifeline} on {@link Interaction}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testLifelineDropOnInteraction() {
        Lifeline lifeline = this.create(Lifeline.class);
        Interaction interaction = this.create(Interaction.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(lifeline,
                interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(lifeline), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Lifeline} on {@link Lifeline}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testLifelineDropOnLifeline() {
        Lifeline lifelineToDrop = this.create(Lifeline.class);
        Lifeline lifeline = this.create(Lifeline.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(lifelineToDrop,
                lifeline, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Message} on {@link Interaction}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testMessageDropOnInteraction() {
        Message message = this.create(Message.class);
        Interaction interaction = this.create(Interaction.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(message,
                interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(message), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Message} on {@link Lifeline}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testMessageDropOnLifeline() {
        Message message = this.create(Message.class);
        Lifeline lifeline = this.create(Lifeline.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(message, lifeline,
                this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(message), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Property} on {@link Interaction}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testPropertyDropOnInteraction() {
        Property property = this.create(Property.class);
        Interaction interaction = this.create(Interaction.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(property,
                interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link Property} on {@link Lifeline}.
     *
     * Expected result : Lifeline.represents = Property.
     */
    @Test
    public void testPropertyDropOnLifeline() {
        Property property = this.create(Property.class);
        Lifeline lifeline = this.create(Lifeline.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(property,
                lifeline, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
        assertEquals(property, lifeline.getRepresents());
    }

    /**
     * Test dropping a {@link TimeObservation} on {@link Interaction}.
     *
     * Expected result : nothing done semantically but a graphical element should be
     * displayed.
     */
    @Test
    public void testTimeObservationDropOnInteraction() {
        TimeObservation timeObservation = this.create(TimeObservation.class);
        Interaction interaction = this.create(Interaction.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(timeObservation,
                interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(timeObservation), status.getElementsToDisplay());
    }

    /**
     * Test dropping a {@link TimeObservation} on {@link Lifeline}.
     *
     * Expected result : DnD failed.
     */
    @Test
    public void testTimeObservationDropOnLifeline() {
        TimeObservation timeObservation = this.create(TimeObservation.class);
        Lifeline lifeline = this.create(Lifeline.class);

        DnDStatus status = new CommunicationExternalSourceToRepresentationDropBehaviorProvider().drop(timeObservation,
                lifeline, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

}
