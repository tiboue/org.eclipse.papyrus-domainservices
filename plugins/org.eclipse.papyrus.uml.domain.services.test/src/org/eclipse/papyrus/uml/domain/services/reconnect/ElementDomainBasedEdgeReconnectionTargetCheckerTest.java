/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.CommunicationPath;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.CreateObjectAction;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentTarget;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InitialNode;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.MergeNode;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.ReclassifyObjectAction;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeReconnectionTargetChecker}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class ElementDomainBasedEdgeReconnectionTargetCheckerTest extends AbstractUMLTest {

    private static final String OWNED_ATTRIBUTE = "ownedAttribute";

    private ElementDomainBasedEdgeReconnectionTargetChecker elementDomainBasedEdgeReconnectionTargetChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.elementDomainBasedEdgeReconnectionTargetChecker = new ElementDomainBasedEdgeReconnectionTargetChecker(
                e -> true, new MockedViewQuerier());
    }

    /**
     * Check that the reconnect target is not authorized when we try to reconnect an
     * association from a class to an other classifier which cannot contain
     * attribute (target property is not owned by the association).
     */
    @Test
    public void testCannotReconnectAssociationTargetWithPropertyNotOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classSource = this.createIn(Class.class, model);
        Class classOldTarget = this.createIn(Class.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);

        // change owner of target Property
        Property targetProperty = association.getMemberEnds().get(1);
        classOldTarget.getOwnedAttributes().add(targetProperty);
        Actor actorNewTarget = this.createIn(Actor.class, model);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association,
                classOldTarget, actorNewTarget, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Check that the reconnect target is authorized when we try to reconnect an
     * association from a class to an other classifier which can contain attribute
     * (target property is not owned by the association).
     */
    @Test
    public void testCanReconnectAssociationTargetWithPropertyNotOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classSource = this.createIn(Class.class, model);
        Class classOldTarget = this.createIn(Class.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classSource, classOldTarget, null, null, null);

        // change owner of target Property
        Property targetProperty = association.getMemberEnds().get(1);
        classOldTarget.getOwnedAttributes().add(targetProperty);
        Class classNewTarget = this.createIn(Class.class, model);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association,
                classOldTarget, classNewTarget, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Check that the reconnect target is authorized when we try to reconnect an
     * association from a class to an other class (target property is owned by the
     * association).
     */
    @Test
    public void testCanReconnectAssociationTargetWithPropertyOwnedByAssociation() {
        Model model = this.create(Model.class);
        UseCase sourceUseCase = this.createIn(UseCase.class, model);
        UseCase oldTargetUseCase = this.createIn(UseCase.class, model);
        UseCase newTargetUseCase = this.createIn(UseCase.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, sourceUseCase, oldTargetUseCase, null, null,
                null);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association,
                oldTargetUseCase, newTargetUseCase, null, null);
        assertTrue(status.isValid());

        Comment newTargetComment = this.createIn(Comment.class, model);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association, oldTargetUseCase,
                newTargetComment, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with a
     * {@link DeploymentTarget} and {@link Classifier} element as target is allowed.
     */
    @Test
    public void testCanReconnectCommunicationPathTargetWithDeploymentTargetClassifiers() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);

        Node newTarget = this.create(Node.class);
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(communicationPath,
                target, newTarget, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with a
     * {@link Classifier}, non-{@link DeploymentTarget} element as target is not
     * allowed.
     */
    @Test
    public void testCanReconnectCommunicationPathTargetWithClassifierNonDeploymentTarget() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);

        Class newTarget = this.create(Class.class);
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(communicationPath,
                target, newTarget, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with a
     * {@link DeploymentTarget}, non-{@link Classifier} element as target is not
     * allowed.
     */
    @Test
    public void testCanReconnectCommunicationPathTargetWithDeploymentTargetNonClassifier() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);

        InstanceSpecification newTarget = this.create(InstanceSpecification.class);
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(communicationPath,
                target, newTarget, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with {@code null} as
     * target is allowed.
     */
    @Test
    public void testCanReconnectCommunicationPathTargetWithNull() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(communicationPath,
                target, null, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with a component
     * element as target is allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationTargetWithComponent() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);

        Component newTarget = this.create(Component.class);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(componentRealization,
                targetComponent, newTarget, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with a non-component
     * source is not allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationTargetWithNonComponent() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);

        Comment newSource = this.create(Comment.class);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(componentRealization,
                targetComponent, newSource, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with a null target is
     * not allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationTargetWithNullTarget() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(componentRealization,
                targetComponent, null, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with a source element
     * as target is not allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationTargetWithSourceElementAsTarget() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Component sourceComponent = this.create(Component.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceComponent);
        componentRealization.setAbstraction(targetComponent);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(componentRealization,
                targetComponent, sourceComponent, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Check that the reconnect is authorized when we try to reconnect a connector
     * between two different properties not contained in each other.
     */
    @Test
    public void testCanReconnectConnectorTargetFrom2DifferentProperty() {
        // create semantic elements
        Class clazz = this.create(Class.class);
        Property propertySource = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property propertyTarget = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property oldProperyTarget = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = this.create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertySource = classNode.addChildren(propertySource);
        VisualNode visualPropertyTarget = classNode.addChildren(propertyTarget);

        // check reconnect is authorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldProperyTarget, propertyTarget, visualPropertySource, visualPropertyTarget);
        assertTrue(canReconnectStatus.isValid());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * from a port view to its self.
     */
    @Test
    public void testCanReconnectConnectorTargetFromPortOnItself() {
        // create semantic elements
        Class clazz = this.create(Class.class);
        Port newPortTarget = this.createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Port oldPortTarget = this.createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = this.createIn(Connector.class, clazz);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPort = classNode.addChildren(newPortTarget);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldPortTarget, newPortTarget, visualPort, visualPort);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot connect a port to itself.", canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a port view and a property view with the port as border node.
     */
    @Test
    public void testCanReconnectConnectorTargetFromPortOnItsPropertyContainerNode() {
        // create semantic elements
        Class type1 = this.create(Class.class);
        Property newPropertyTarget = this.create(Property.class);
        newPropertyTarget.setType(type1);
        Port sourcePort = this.createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Port oldPortTarget = this.createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Connector connector = this.create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode visualNewPropertyTarget = visualTree.addChildren(newPropertyTarget);
        VisualNode visualSourcePort = visualNewPropertyTarget.addBorderNode(sourcePort);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldPortTarget, newPropertyTarget, visualSourcePort, visualNewPropertyTarget);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot create a connector from a view representing a Part to its own Port (or the opposite).",
                canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a property view contained by an other one.
     */
    @Test
    public void testCanReconnectConnectorTargetFromPropertyContainedInAnotherOne() {
        // create semantic elements
        Class type1 = this.create(Class.class);
        Class clazz = this.create(Class.class);
        Property oldPropertyTarget = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        oldPropertyTarget.setType(type1);
        Property propertySource = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        propertySource.setType(type1);
        Property newPropertyTarget = this.createIn(Property.class, type1, OWNED_ATTRIBUTE);
        Connector connector = this.create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertySource = classNode.addChildren(propertySource);
        VisualNode visualNewPropertyTarget = visualPropertySource.addChildren(newPropertyTarget);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(connector,
                oldPropertyTarget, newPropertyTarget, visualPropertySource, visualNewPropertyTarget);
        assertFalse(canReconnectStatus.isValid());
        assertEquals(
                "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.",
                canReconnectStatus.getMessage());
    }

    /**
     * Check the canReconnect with {@link ControlFlow}. This test aims to check if
     * the checker mechanism is properly called. This test will not verify all cases
     * since they are already verified by the
     * ElementDomainBasedEdgeCreationCheckerTest.testCanCreateControlFlowTarget
     * test. The
     * org.eclipse.papyrus.uml.domain.services.objectFlow.ControlFlowHelper.canCreateControlFlow
     * method is called by both ElementDomainBasedEdgeCreationChecker and
     * ElementDomainBasedEdgeReconnectionTargetChecker.
     */
    @Test
    public void testCanReconnectControlFlowTarget() {
        // Check the basic case
        MergeNode mergeNode = this.create(MergeNode.class);
        OpaqueAction opaqueAction = this.create(OpaqueAction.class);
        OpaqueAction newOpaqueAction = this.create(OpaqueAction.class);
        ControlFlow controlFlow = this.create(ControlFlow.class);
        controlFlow.setSource(mergeNode);
        controlFlow.setTarget(opaqueAction);
        // Check an authorize case.
        assertTrue(new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(controlFlow, opaqueAction, newOpaqueAction, null, null).isValid());

        // Check a forbidden case.
        Activity activity = this.create(Activity.class);
        assertFalse(new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(controlFlow, opaqueAction, activity, null, null).isValid());
    }

    /**
     * Test reconnect target with a named element => reconnection is authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNamedElement() {
        // create semantic elements
        Dependency dependency = this.create(Dependency.class);
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Class c3 = this.create(Class.class);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2,
                c3, null, null);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Test reconnect target with no namedElement => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNoNamedElement() {
        // create semantic elements
        Dependency dependency = this.create(Dependency.class);
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Comment comment = this.create(Comment.class);

        // check creation is unauthorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2,
                comment, null, null);
        assertFalse(canCreateStatus.isValid());
        assertEquals("Dependency target can only be reconnected to a non null NamedElement.",
                canCreateStatus.getMessage());
    }

    /**
     * Test reconnect target with null target => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNullTarget() {
        // create semantic elements
        Dependency dependency = this.create(Dependency.class);
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2,
                null, null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect target with a {@link DeploymentTarget} target => reconnection
     * is authorized.
     */
    @Test
    public void testCanReconnectDeploymentTargetWithDeploymentTarget() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact = this.create(Artifact.class);
        Node node1 = this.create(Node.class);
        Node node2 = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact);
        deployment.setLocation(node1);

        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(deployment,
                node1, node2, null, null);
        assertTrue(canReconnectStatus.isValid());
    }

    /**
     * Test reconnect target with a non-{@link DeploymentTarget} target =>
     * reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDeploymentTargetWithNonDeploymentTarget() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact = this.create(Artifact.class);
        Node node1 = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact);
        deployment.setLocation(node1);

        Comment comment = this.create(Comment.class);

        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(deployment,
                node1, comment, null, null);
        assertFalse(canReconnectStatus.isValid());
    }

    /**
     * Test reconnect target with a {@code null} target => reconnection is not
     * authorized.
     */
    @Test
    public void testCanReconnectDeploymentTargetWithNullTarget() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact = this.create(Artifact.class);
        Node node1 = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact);
        deployment.setLocation(node1);

        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(deployment,
                node1, null, null, null);
        assertFalse(canReconnectStatus.isValid());
    }

    /**
     * Default reconnection for {@link Extend}.
     */
    @Test
    public void testCanReconnectExtendTarget() {
        // create semantic elements
        Extend extend = this.create(Extend.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        extend.setExtension(source);
        extend.setExtendedCase(target);
        UseCase newTarget = this.create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extend, target,
                newTarget, null, null);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extend, target, source, null, null);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = this.create(Package.class);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extend, target, newTargetPackage,
                null, null);
        assertFalse(status.isValid());
    }

    /**
     * Test can reconnect the {@link Extension} target.
     */
    @Test
    public void testCanReconnectExtensionTarget() {
        Stereotype stereotype = this.create(Stereotype.class);
        PrimitiveType primitiveType = this.create(PrimitiveType.class);
        Class class1 = this.create(Class.class);
        Class class2 = this.create(Class.class);
        Extension extension = this.create(Extension.class);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        CheckStatus canReconnect = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extension, class1,
                class2, null, null);
        assertTrue(canReconnect.isValid());
        canReconnect = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(extension, class1,
                primitiveType, null, null);
        assertFalse(canReconnect.isValid());
    }

    /**
     * Default reconnection for Generalization.
     */
    @Test
    public void testCanReconnectGeneralizationTarget() {
        // create semantic elements
        Generalization generalization = this.create(Generalization.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newTarget = this.create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target,
                newTarget, null, null);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target, source, null,
                null);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = this.create(Package.class);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target,
                newTargetPackage, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Include}.
     */
    @Test
    public void testCanReconnectIncludeTarget() {
        // create semantic elements
        Include include = this.create(Include.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newTarget = this.create(UseCase.class);

        // check reconnection on UseCase
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target,
                newTarget, null, null);
        assertTrue(status.isValid());

        // cannot reconnect on the source
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, source, null, null);
        assertFalse(status.isValid());

        // cannot reconnect on non {@link UseCase}
        Actor newErrorTarget = this.create(Actor.class);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, newErrorTarget,
                null, null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link InformationFlow}.
     */
    @Test
    public void testCanReconnectInformationFlowTarget() {
        Class target = this.create(Class.class);
        Class newTarget = this.create(Class.class);
        Package package1 = this.create(Package.class);
        Comment comment = this.create(Comment.class);
        InformationFlow informationFlow = this.create(InformationFlow.class);
        informationFlow.getInformationTargets().add(target);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false,
                new MockedViewQuerier()).canReconnect(informationFlow, target, newTarget, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow is not contained in a Package.");
        package1.getPackagedElements().add(informationFlow);
        canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, target, newTarget, null, null);
        assertTrue(canCreateStatus.isValid());
        canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, target, comment, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow new target should be a NamedElement");
    }

    /**
     * Default reconnection for {@link Manifestation}.
     */
    @Test
    public void testCanReconnectManifestationTarget() {
        Class target = this.create(Class.class);
        Class newTarget = this.create(Class.class);
        Include include = this.create(Include.class);
        Manifestation manifestation = this.create(Manifestation.class);
        manifestation.setUtilizedElement(target);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false,
                new MockedViewQuerier()).canReconnect(manifestation, target, include, null, null);
        assertFalse(canCreateStatus.isValid(), "the target should be a PackageableElement");
        canCreateStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(manifestation, target, newTarget, null, null);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Default reconnection for {@link Message}.
     */
    @Test
    public void testCanReconnectMessageTarget() {
        Lifeline target = this.create(Lifeline.class);
        Lifeline newTarget = this.create(Lifeline.class);
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        CheckStatus canReconnectStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false,
                new MockedViewQuerier()).canReconnect(message, target, interaction, null, null);
        assertFalse(canReconnectStatus.isValid(), "the target should be a Lifeline");
        canReconnectStatus = new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(message, target, newTarget, null, null);
        assertTrue(canReconnectStatus.isValid());
    }

    /**
     * Check the canReconnect with ObjectFlow. This test aims to check if the
     * checker mechanism is properly called. This test will not verify all cases
     * since they are already verified by the
     * ElementDomainBasedEdgeCreationCheckerTest.testCanCreateObjectFlowTarget test.
     * The
     * org.eclipse.papyrus.uml.domain.services.objectFlow.ObjectFlowHelper.canCreateObjectFlow
     * method is called by both ElementDomainBasedEdgeCreationChecker and
     * ElementDomainBasedEdgeReconnectionTargetChecker.
     */
    @Test
    public void testCanReconnectObjectFlowTarget() {
        CreateObjectAction createObjectAction = this.create(CreateObjectAction.class);
        ReclassifyObjectAction reclassifyObjectAction = this.create(ReclassifyObjectAction.class);
        OutputPin outputPin = this.createIn(OutputPin.class, createObjectAction);
        InputPin inputPin = this.createIn(InputPin.class, reclassifyObjectAction);
        InputPin newInputPin = this.createIn(InputPin.class, reclassifyObjectAction);
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        objectFlow.setSource(outputPin);
        objectFlow.setTarget(inputPin);

        // Check an authorize case.
        assertTrue(new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(objectFlow, inputPin, newInputPin, null, null).isValid());

        // Check a forbidden case.
        InitialNode initialNode = this.create(InitialNode.class);
        assertFalse(new ElementDomainBasedEdgeReconnectionTargetChecker(e -> false, new MockedViewQuerier())
                .canReconnect(objectFlow, inputPin, initialNode, null, null).isValid());
    }

    /**
     * Default reconnection for PackageImport.
     */
    @Test
    public void testCanReconnectPackageImportTarget() {

        Package packSource = this.create(Package.class);
        Package packTarget = this.create(Package.class);
        Package packTarget2 = this.create(Package.class);

        PackageImport packImport = this.createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(packImport, packTarget,
                packTarget2, null, null);
        assertTrue(status.isValid());

        // can reconnect on NameSpace
        Comment errorTarget = this.create(Comment.class);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(packImport, packTarget, errorTarget,
                null, null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for PackageMerge.
     */
    @Test
    public void testCanReconnectPackageMergeTarget() {

        Package packSource = this.create(Package.class);
        Package packTarget = this.create(Package.class);
        Package packTarget2 = this.create(Package.class);

        PackageMerge merge = this.createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(merge, packTarget,
                packTarget2, null, null);
        assertTrue(status.isValid());

        // can't reconnect on non Package
        Actor errorTarget = this.create(Actor.class);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(merge, packTarget, errorTarget, null,
                null);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Substitution}.
     */
    @Test
    public void testCanReconnectSubstitutionTarget() {
        // create semantic elements
        Substitution substitution = this.create(Substitution.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        substitution.setSubstitutingClassifier(source);
        substitution.setContract(target);
        UseCase newTarget = this.create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target,
                newTarget, null, null);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target, source, null,
                null);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = this.create(Package.class);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target,
                newTargetPackage, null, null);
        assertFalse(status.isValid());
    }

    @Test
    public void testCanReconnectTransitionTarget() {
        State state1 = this.create(State.class);
        Pseudostate source = this.createIn(Pseudostate.class, state1);
        Region region = this.createIn(Region.class, state1);
        Pseudostate target1 = this.createIn(Pseudostate.class, region);
        Pseudostate target2 = this.createIn(Pseudostate.class, region);

        Transition transition = this.createIn(Transition.class, region);
        transition.setSource(source);
        transition.setTarget(target1);

        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(transition,
                target1, target2, null, null);
        assertTrue(canCreateStatus.isValid());

        canCreateStatus = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(transition, target1, region,
                null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Default reconnection for {@link Usage}.
     */
    @Test
    public void testCanReconnectUsageTarget() {
        // create semantic elements
        Usage usage = this.create(Usage.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        usage.getClients().add(source);
        usage.getSuppliers().add(target);
        UseCase newTarget = this.create(UseCase.class);

        // test reconnection on NamedElement
        CheckStatus status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(usage, target, newTarget,
                null, null);
        assertTrue(status.isValid());

        // test reconnection on non NamedElement
        Comment newTargetComment = this.create(Comment.class);
        status = this.elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(usage, target, newTargetComment,
                null, null);
        assertFalse(status.isValid());
    }
}
