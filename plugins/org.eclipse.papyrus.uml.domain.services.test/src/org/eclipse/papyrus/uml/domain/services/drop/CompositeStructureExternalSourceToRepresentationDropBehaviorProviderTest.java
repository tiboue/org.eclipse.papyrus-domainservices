/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CompositeStructureExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.StateMachine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for
 * {@link CompositeStructureExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class CompositeStructureExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private CompositeStructureExternalSourceToRepresentationDropBehaviorProvider compositeStructureExternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider = new CompositeStructureExternalSourceToRepresentationDropBehaviorProvider();
    }

    /**
     * Test dropping a Class on Property => Propert.type = Class.
     */
    @Test
    public void testClassDropOnProperty() {
        Property property = this.create(Property.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        DnDStatus status = this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider.drop(clazz,
                property, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(clazz, property.getType());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    @Test
    public void testCollaborationDropOnCollaborationUse() {
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        Collaboration collaboration = this.create(Collaboration.class);

        DnDStatus status = this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider.drop(collaboration,
                collaborationUse, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertEquals(collaboration, collaborationUse.getType());
    }

    /**
     * Test dropping a DataType on Property => Propert.type = DataType.
     */
    @Test
    public void testDataTypeDropOnProperty() {
        Property property = this.create(Property.class);
        DataType dt = this.create(DataType.class);

        DnDStatus status = this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider.drop(dt, property,
                this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertEquals(dt, property.getType());
        assertEquals(Collections.emptySet(), status.getElementsToDisplay());
    }

    private void testDragAndDrop(Class<? extends Element> classToDrop, Class<? extends Element> containingClass,
            boolean expectedResult) {
        Element elementToDrop = this.create(classToDrop);
        Element containingElement = this.create(containingClass);

        DnDStatus status = this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider.drop(elementToDrop,
                containingElement, this.getCrossRef(), this.getEditableChecker());
        State expectedState = State.FAILED;
        if (expectedResult) {
            assertTrue(List.of(elementToDrop).containsAll(status.getElementsToDisplay()));
            expectedState = State.NOTHING;
        }
        assertEquals(expectedState, status.getState());
    }

    /**
     * Test dropping an element into another.
     * <p>
     * This test case runs all the tests where a {@link Classifier} is dropped.
     * Non-classifier drops are tested in
     * {@link #testDragAndDropNonClassifiers(Class, Class, boolean)}.
     * </p>
     *
     * @param classToDrop
     *                        the type of the element to drop
     * @param containingClass
     *                        the type of the element containing the element to drop
     * @param expectedResult
     *                        true if NOTHING is expected, false if FAILED is
     *                        expected.
     *
     */
    @ParameterizedTest
    @MethodSource("getDragAndDropParametersClassifiers")
    public void testDragAndDropClassifiers(Class<? extends Element> classToDrop,
            Class<? extends Element> containingClass, boolean expectedResult) {
        this.testDragAndDrop(classToDrop, containingClass, expectedResult);
    }

    /**
     * Test dropping an element into another.
     * <p>
     * This test case runs all the tests where a non-{@link Classifier} is dropped.
     * Classifier drops are tested in
     * {@link #testDragAndDropClassifiers(Class, Class, boolean)}.
     * </p>
     *
     * @param classToDrop
     *                        the type of the element to drop
     * @param containingClass
     *                        the type of the element containing the element to drop
     * @param expectedResult
     *                        true if NOTHING is expected, false if FAILED is
     *                        expected.
     *
     */
    @ParameterizedTest
    @MethodSource("getDragAndDropParametersNonClassifiers")
    public void testDragAndDropNonClassifiers(Class<? extends Element> classToDrop,
            Class<? extends Element> containingClass, boolean expectedResult) {
        this.testDragAndDrop(classToDrop, containingClass, expectedResult);
    }

    @Test
    public void testPortDropOnTypedPort() {
        Port port = this.create(Port.class);
        Port typedPort = this.create(Port.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);
        typedPort.setType(clazz);

        DnDStatus status = this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider.drop(port,
                typedPort, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(port), status.getElementsToDisplay());
    }

    @Test
    public void testPortDropOnTypedProperty() {
        Port port = this.create(Port.class);
        Property typedProperty = this.create(Property.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);
        typedProperty.setType(clazz);

        DnDStatus status = this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider.drop(port,
                typedProperty, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(port), status.getElementsToDisplay());
    }

    @Test
    public void testPropertyDropOnTypedProperty() {
        Property property = this.create(Property.class);
        Property typedProperty = this.create(Property.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);
        typedProperty.setType(clazz);

        DnDStatus status = this.compositeStructureExternalSourceToRepresentationDropBehaviorProvider.drop(property,
                typedProperty, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.NOTHING, status.getState());
        assertEquals(Set.of(property), status.getElementsToDisplay());
    }
    
    public static Stream<Arguments> getDragAndDropParametersClassifiers() {
        // Classifier and non-classifier parameters are split to avoid checkstyle
        // issues.
        // @formatter:off
        return Stream.of(
                Arguments.of(Activity.class, Package.class, true),
                Arguments.of(Activity.class, Comment.class, false),
                Arguments.of(Activity.class, Activity.class, true),
                Arguments.of(Activity.class, Class.class, true),
                Arguments.of(Activity.class, FunctionBehavior.class, true),
                Arguments.of(Activity.class, Interaction.class, true),
                Arguments.of(Activity.class, OpaqueBehavior.class, true),
                Arguments.of(Activity.class, ProtocolStateMachine.class, true),
                Arguments.of(Activity.class, StateMachine.class, true),
                Arguments.of(Class.class, Package.class, true),
                Arguments.of(Class.class, Comment.class, false),
                Arguments.of(Class.class, Activity.class, true),
                Arguments.of(Class.class, Class.class, true),
                Arguments.of(Class.class, FunctionBehavior.class, true),
                Arguments.of(Class.class, Interaction.class, true),
                Arguments.of(Class.class, OpaqueBehavior.class, true),
                Arguments.of(Class.class, ProtocolStateMachine.class, true),
                Arguments.of(Class.class, StateMachine.class, true),
                Arguments.of(Collaboration.class, Package.class, true),
                Arguments.of(Collaboration.class, Comment.class, false),
                Arguments.of(Collaboration.class, Activity.class, true),
                Arguments.of(Collaboration.class, Class.class, true),
                Arguments.of(Collaboration.class, FunctionBehavior.class, true),
                Arguments.of(Collaboration.class, Interaction.class, true),
                Arguments.of(Collaboration.class, OpaqueBehavior.class, true),
                Arguments.of(Collaboration.class, ProtocolStateMachine.class, true),
                Arguments.of(Collaboration.class, StateMachine.class, true),
                Arguments.of(FunctionBehavior.class, Package.class, true),
                Arguments.of(FunctionBehavior.class, Comment.class, false),
                Arguments.of(FunctionBehavior.class, Activity.class, true),
                Arguments.of(FunctionBehavior.class, Class.class, true),
                Arguments.of(FunctionBehavior.class, FunctionBehavior.class, true),
                Arguments.of(FunctionBehavior.class, Interaction.class, true),
                Arguments.of(FunctionBehavior.class, OpaqueBehavior.class, true),
                Arguments.of(FunctionBehavior.class, ProtocolStateMachine.class, true),
                Arguments.of(FunctionBehavior.class, StateMachine.class, true),
                Arguments.of(InformationItem.class, Package.class, true),
                Arguments.of(InformationItem.class, Comment.class, false),
                Arguments.of(InformationItem.class, Activity.class, true),
                Arguments.of(InformationItem.class, Class.class, true),
                Arguments.of(InformationItem.class, FunctionBehavior.class, true),
                Arguments.of(InformationItem.class, Interaction.class, true),
                Arguments.of(InformationItem.class, OpaqueBehavior.class, true),
                Arguments.of(InformationItem.class, ProtocolStateMachine.class, true),
                Arguments.of(InformationItem.class, StateMachine.class, true),
                Arguments.of(Interaction.class, Package.class, true),
                Arguments.of(Interaction.class, Comment.class, false),
                Arguments.of(Interaction.class, Activity.class, true),
                Arguments.of(Interaction.class, Class.class, true),
                Arguments.of(Interaction.class, FunctionBehavior.class, true),
                Arguments.of(Interaction.class, Interaction.class, true),
                Arguments.of(Interaction.class, OpaqueBehavior.class, true),
                Arguments.of(Interaction.class, ProtocolStateMachine.class, true),
                Arguments.of(Interaction.class, StateMachine.class, true),
                Arguments.of(OpaqueBehavior.class, Package.class, true),
                Arguments.of(OpaqueBehavior.class, Comment.class, false),
                Arguments.of(OpaqueBehavior.class, Activity.class, true),
                Arguments.of(OpaqueBehavior.class, Class.class, true),
                Arguments.of(OpaqueBehavior.class, FunctionBehavior.class, true),
                Arguments.of(OpaqueBehavior.class, Interaction.class, true),
                Arguments.of(OpaqueBehavior.class, OpaqueBehavior.class, true),
                Arguments.of(OpaqueBehavior.class, ProtocolStateMachine.class, true),
                Arguments.of(OpaqueBehavior.class, StateMachine.class, true),
                // Property drop on typed property is tested in individual test case.
                Arguments.of(ProtocolStateMachine.class, Package.class, true),
                Arguments.of(ProtocolStateMachine.class, Comment.class, false),
                Arguments.of(ProtocolStateMachine.class, Activity.class, true),
                Arguments.of(ProtocolStateMachine.class, Class.class, true),
                Arguments.of(ProtocolStateMachine.class, FunctionBehavior.class, true),
                Arguments.of(ProtocolStateMachine.class, Interaction.class, true),
                Arguments.of(ProtocolStateMachine.class, OpaqueBehavior.class, true),
                Arguments.of(ProtocolStateMachine.class, ProtocolStateMachine.class, true),
                Arguments.of(ProtocolStateMachine.class, StateMachine.class, true),
                Arguments.of(StateMachine.class, Package.class, true),
                Arguments.of(StateMachine.class, Comment.class, false),
                Arguments.of(StateMachine.class, Activity.class, true),
                Arguments.of(StateMachine.class, Class.class, true),
                Arguments.of(StateMachine.class, FunctionBehavior.class, true),
                Arguments.of(StateMachine.class, Interaction.class, true),
                Arguments.of(StateMachine.class, OpaqueBehavior.class, true),
                Arguments.of(StateMachine.class, ProtocolStateMachine.class, true),
                Arguments.of(StateMachine.class, StateMachine.class, true),
                Arguments.of(Artifact.class, Package.class, false)
                );
        // @formatter:on
    }

    public static Stream<Arguments> getDragAndDropParametersNonClassifiers() {
        // Classifier and non-classifier parameters are split to avoid checkstyle
        // issues.
        // @formatter:off
        return Stream.of(
                Arguments.of(CollaborationUse.class, Comment.class, false),
                Arguments.of(CollaborationUse.class, Activity.class, true),
                Arguments.of(CollaborationUse.class, Class.class, true),
                Arguments.of(CollaborationUse.class, Collaboration.class, true),
                Arguments.of(CollaborationUse.class, FunctionBehavior.class, true),
                Arguments.of(CollaborationUse.class, Interaction.class, true),
                Arguments.of(CollaborationUse.class, OpaqueBehavior.class, true),
                Arguments.of(CollaborationUse.class, ProtocolStateMachine.class, true),
                Arguments.of(CollaborationUse.class, StateMachine.class, true),
                Arguments.of(Comment.class, Package.class, true),
                Arguments.of(Comment.class, Comment.class, false),
                Arguments.of(Comment.class, Activity.class, true),
                Arguments.of(Comment.class, Class.class, true),
                Arguments.of(Comment.class, Collaboration.class, true),
                Arguments.of(Comment.class, FunctionBehavior.class, true),
                Arguments.of(Comment.class, Interaction.class, true),
                Arguments.of(Comment.class, OpaqueBehavior.class, true),
                Arguments.of(Comment.class, Property.class, true),
                Arguments.of(Comment.class, ProtocolStateMachine.class, true),
                Arguments.of(Comment.class, StateMachine.class, true),
                Arguments.of(Connector.class, StateMachine.class, true),
                Arguments.of(Constraint.class, Package.class, true),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(Constraint.class, Activity.class, true),
                Arguments.of(Constraint.class, Class.class, true),
                Arguments.of(Constraint.class, Collaboration.class, true),
                Arguments.of(Constraint.class, FunctionBehavior.class, true),
                Arguments.of(Constraint.class, Interaction.class, true),
                Arguments.of(Constraint.class, OpaqueBehavior.class, true),
                Arguments.of(Constraint.class, ProtocolStateMachine.class, true),
                Arguments.of(Constraint.class, StateMachine.class, true),
                Arguments.of(Generalization.class, StateMachine.class, true),
                Arguments.of(Parameter.class, Package.class, false),
                Arguments.of(Parameter.class, Activity.class, true),
                Arguments.of(Parameter.class, Class.class, false),
                Arguments.of(Parameter.class, Collaboration.class, false),
                Arguments.of(Parameter.class, FunctionBehavior.class, true),
                Arguments.of(Parameter.class, Interaction.class, true),
                Arguments.of(Parameter.class, OpaqueBehavior.class, true),
                Arguments.of(Parameter.class, ProtocolStateMachine.class, true),
                Arguments.of(Parameter.class, StateMachine.class, true),
                Arguments.of(Port.class, Package.class, false),
                Arguments.of(Port.class, Activity.class, true),
                Arguments.of(Port.class, Class.class, true),
                Arguments.of(Port.class, FunctionBehavior.class, true),
                Arguments.of(Port.class, Interaction.class, true),
                Arguments.of(Port.class, OpaqueBehavior.class, true),
                Arguments.of(Port.class, ProtocolStateMachine.class, true),
                Arguments.of(Port.class, StateMachine.class, true),
                // Port drop on typed port / typed property is tested in individual test cases.
                Arguments.of(Property.class, Package.class, false),
                Arguments.of(Property.class, Activity.class, true),
                Arguments.of(Property.class, Class.class, true),
                Arguments.of(Property.class, Collaboration.class, true),
                Arguments.of(Property.class, FunctionBehavior.class, true),
                Arguments.of(Property.class, Interaction.class, true),
                Arguments.of(Property.class, OpaqueBehavior.class, true),
                Arguments.of(Property.class, ProtocolStateMachine.class, true),
                Arguments.of(Property.class, StateMachine.class, true)
                );
     // @formatter:on
    }

}
