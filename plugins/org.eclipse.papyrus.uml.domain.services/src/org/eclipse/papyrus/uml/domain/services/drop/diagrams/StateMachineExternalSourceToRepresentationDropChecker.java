/*****************************************************************************
 * Copyright (c) 2024 CEA LIST, OBEO, Artal Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *  Aurelien Didier (Artal Technologies) - Issue 190
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Checks if a semantic D&D is possible from the Model Explorer to the
 * StateMachine diagram.
 *
 * @author Aurelien Didier
 */
public class StateMachineExternalSourceToRepresentationDropChecker
        implements IExternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new StateMachineDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class StateMachineDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject newSemanticContainer;

        StateMachineDropInsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Region)
                    && !(this.newSemanticContainer instanceof StateMachine)) {
                result = CheckStatus.no("Comment can only be drag and drop on Region or diagram.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseState(State state) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Region)) {
                result = CheckStatus.no("State can only be drag and drop on Region.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseRegion(Region region) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof StateMachine || this.newSemanticContainer instanceof State)) {
                result = CheckStatus.no("Region can only be drag and drop on StateMachine or State.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePseudostate(Pseudostate pseudostate) {
            CheckStatus result = null;
            if (pseudostate.getKind().equals(PseudostateKind.ENTRY_POINT_LITERAL)
                    || pseudostate.getKind().equals(PseudostateKind.EXIT_POINT_LITERAL)) {
                if (!(this.newSemanticContainer instanceof Region || this.newSemanticContainer instanceof StateMachine
                        || this.newSemanticContainer instanceof State)) {
                    result = CheckStatus.no("Entry or Exit Point can only be drag and drop on StateMachine or State.");
                }
            } else if (!(this.newSemanticContainer instanceof Region)) {
                result = CheckStatus.no("This Pseudostate can only be drag and drop on a Region.");
            }
            if (result == null) {
                return CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }
    }
}
