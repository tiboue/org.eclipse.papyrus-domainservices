/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.modify.IFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provides drop behavior for diagram element in the Component diagram.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentInternalSourceToRepresentationDropBehaviorProvider
        implements IInternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public Status drop(EObject droppedElement, EObject oldContainer, EObject newContainer,
            ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        return new ComponentDropOutsideRepresentationBehaviorProviderSwitch(oldContainer, newContainer, crossRef,
                editableChecker).doSwitch(droppedElement);
    }

    static class ComponentDropOutsideRepresentationBehaviorProviderSwitch extends UMLSwitch<Status> {

        private final EObject oldContainer;

        private final EObject newContainer;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        /**
         * Initializes the switch.
         *
         * @param oldContainer
         *                        the old container of the element being dropped
         * @param newContainer
         *                        the new container of the element being dropped
         * @param crossRef
         *                        the cross referencer
         * @param editableChecker
         *                        the checker
         */
        ComponentDropOutsideRepresentationBehaviorProviderSwitch(EObject oldContainer, EObject newContainer,
                ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
            super();
            this.oldContainer = oldContainer;
            this.newContainer = newContainer;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        /**
         * Default Behavior : UML element can be D&D by using the same reference
         * containment.
         *
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseElement(org.eclipse.uml2.uml.Element)
         *
         * @param droppedElement
         *                       the element to drop
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseElement(Element droppedElement) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            if (this.oldContainer != this.newContainer) {
                String refName = droppedElement.eContainmentFeature().getName();
                if (this.oldContainer.eClass().getEStructuralFeature(refName) != null
                        && this.newContainer.eClass().getEStructuralFeature(refName) != null) {
                    dropStatus = modifier.removeValue(this.oldContainer, refName, droppedElement);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(this.newContainer, refName, droppedElement);
                    }
                    return dropStatus;
                }
            }
            return super.caseElement(droppedElement);
        }

        /**
         * Handle the drop of a {@link Property}.
         * <p>
         * This method prevents drag and drops from a {@link Component} to a
         * non-Component, and from an {@link Interface} to a non-Interface.
         *
         * @param droppedProperty
         * @return
         */
        @Override
        public Status caseProperty(Property droppedProperty) {
            IFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            Status dropStatus = null;
            if ((this.oldContainer instanceof Component && this.newContainer instanceof Interface)
                    || (this.oldContainer instanceof Interface && this.newContainer instanceof Component)) {
                dropStatus = Status
                        .createFailingStatus("Cannot drag and drop a Property from an Interface to a Component");
            } else if (this.oldContainer != this.newContainer) {
                EObject oldSemanticContainer = droppedProperty.eContainer();
                if (oldSemanticContainer != null) {
                    String refName = droppedProperty.eContainingFeature().getName();
                    EObject newSemanticContainer = this.getPropertyNewSemanticContainer();
                    if (newSemanticContainer != null) {
                        dropStatus = modifier.removeValue(oldSemanticContainer, refName, droppedProperty);
                        if (State.DONE == dropStatus.getState()) {
                            dropStatus = modifier.addValue(newSemanticContainer,
                                    UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute().getName(),
                                    droppedProperty);
                        }
                    } else {
                        dropStatus = Status.createFailingStatus(
                                "Container should be a Structured Classifier or a Typed Property.");
                    }
                }
            }
            if (dropStatus == null) {
                dropStatus = super.caseProperty(droppedProperty);
            }
            return dropStatus;
        }

        private EObject getPropertyNewSemanticContainer() {
            EObject newSemanticContainer = null;
            if (this.newContainer instanceof Property) {
                Type type = ((Property) this.newContainer).getType();
                if (type instanceof StructuredClassifier) {
                    newSemanticContainer = type;
                }
            } else if (this.newContainer instanceof StructuredClassifier) {
                newSemanticContainer = this.newContainer;
            } else if (this.newContainer instanceof Interface) {
                newSemanticContainer = this.newContainer;
            }
            return newSemanticContainer;
        }

    }

}
