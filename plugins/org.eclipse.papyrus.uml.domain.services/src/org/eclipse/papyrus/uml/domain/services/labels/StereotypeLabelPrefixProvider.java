/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels;

import static java.util.stream.Collectors.joining;

import java.util.function.Function;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Stereotype;

/**
 * Computes the prefix representing all stereotypes applied to a given element.
 * 
 * @author Arthur Daussy
 *
 */
public class StereotypeLabelPrefixProvider implements Function<EObject, String> {

    @Override
    public String apply(EObject t) {
        if (t instanceof Element) {
            Element element = (Element) t;
            EList<Stereotype> appliedStereotypes = element.getAppliedStereotypes();
            if (!appliedStereotypes.isEmpty()) {
                return appliedStereotypes.stream().map(s -> s.getName())
                        .collect(joining(", ", UMLCharacters.ST_LEFT, UMLCharacters.ST_RIGHT));
            }
        }
        return null;
    }

}
