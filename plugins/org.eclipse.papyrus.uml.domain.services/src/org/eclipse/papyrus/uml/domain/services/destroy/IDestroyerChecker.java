/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;

/**
 * Tells if an object can be destroyed.
 *
 * @author lfasani
 */
public interface IDestroyerChecker {

    /**
     * Checks if the trees of semantic objects can be destroyed.
     *
     * @param objectsToDelete
     *                        the objects that are about to be destroyed and their
     *                        contained elements.
     */
    DestroyerStatus canDestroy(Set<EObject> objectsToDelete);

}
