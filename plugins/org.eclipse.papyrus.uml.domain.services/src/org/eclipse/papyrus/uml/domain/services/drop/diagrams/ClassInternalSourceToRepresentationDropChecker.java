/*****************************************************************************
 * Copyright (c) 2024 CEA LIST, OOBEObeo, Artal Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *  Aurelien Didier (Artal Technologies) - Issue 190
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Checks if a graphical D&D is possible in the Class diagram.
 *
 * @author Aurelien Didier
 */
public class ClassInternalSourceToRepresentationDropChecker implements IInternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new ClassDropOutsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class ClassDropOutsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private EObject newSemanticContainer;

        ClassDropOutsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseInterface(Interface inter) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseSignal(Signal signal) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseClass(Class clazz) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseDataType(DataType dataType) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseEnumeration(Enumeration enumeration) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseEnumerationLiteral(EnumerationLiteral enumerationLiteral) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Enumeration)) {
                result = CheckStatus.no("EnumerationLiteral can only be drag and drop on Enumeration.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseReception(Reception reception) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Class) && !(this.newSemanticContainer instanceof Interface)) {
                result = CheckStatus.no("Reception can only be drag and drop on Class or Interface.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseOperation(Operation operation) {
            final CheckStatus result;
            // The Operation Drop is authorized over Class, DataType but not over
            // Enumeration.
            boolean isOperationHolder = this.newSemanticContainer instanceof org.eclipse.uml2.uml.Class
                    || this.newSemanticContainer instanceof Interface || this.newSemanticContainer instanceof DataType;
            if (!isOperationHolder || this.newSemanticContainer instanceof Enumeration) {
                result = CheckStatus.no(
                        "Operation can only be drag and drop on a Class, an Interface, a PrimitiveType or a DataType.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackage(Package pack) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus casePrimitiveType(PrimitiveType primitiveType) {
            return this.handlePackageContainer();
        }

        @Override
        public CheckStatus caseProperty(Property property) {
            final CheckStatus result;
            // The Property Drop is authorized over Class, DataType but not over
            // Enumeration.
            boolean isPropertyHolder = this.newSemanticContainer instanceof org.eclipse.uml2.uml.Class
                    || this.newSemanticContainer instanceof Interface || this.newSemanticContainer instanceof DataType
                    || this.newSemanticContainer instanceof Signal;
            if (!isPropertyHolder || this.newSemanticContainer instanceof Enumeration) {
                result = CheckStatus.no(
                        "Property can only be drag and drop on a Class, an Interface, a DataType, a PrimitiveType or a Signal.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }

        private CheckStatus handlePackageContainer() {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Package)) {
                result = CheckStatus.no(MessageFormat.format("{0} can only be drag and drop on a Package kind element.",
                        this.newSemanticContainer.eClass().getName()));
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }
    }
}
