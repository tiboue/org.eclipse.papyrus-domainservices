/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.modify;

/**
 * Define the type of the modification.
 * 
 * @author Arthur Daussy
 *
 */
public enum ModificationType {

    /**
     * Completely set the value of a feature (override its current value even for
     * multivalued feature).
     */
    SET,
    /**
     * Adds a new value to a feature. If the feature override only one value then
     * override it.
     */
    ADD,
    /**
     * Remove a value from a list of existing value.
     */
    REMOVE

}
