/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityGroup;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExpansionNode;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.InterruptibleActivityRegion;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * SourceToRepresentationDropChecker for Activity diagram.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class ActivityInternalSourceToRepresentationDropChecker implements IInternalSourceToRepresentationDropChecker {

    private static final String UNAUTHORIZED_DND_ERROR_MSG = "DnD is not authorized.";

    private static final String DROP_ERROR_MSG = "{0} can only be drag and drop on {1}.";

    private static final String COMA = ", ";

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new ActivityDropOutsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class ActivityDropOutsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject newSemanticContainer;

        ActivityDropOutsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseActivity(Activity activity) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Activity) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, activity.eClass().getName(),
                        UMLPackage.eINSTANCE.getActivity().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseActivityNode(ActivityNode activityNode) {
            CheckStatus result = null;
            if (this.newSemanticContainer instanceof Activity || this.newSemanticContainer instanceof ActivityGroup) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, activityNode.eClass().getName(),
                        UMLPackage.eINSTANCE.getActivity().getName() + COMA
                                + UMLPackage.eINSTANCE.getActivityGroup().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseActivityParameterNode(ActivityParameterNode activityParameterNode) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Activity) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, activityParameterNode.eClass().getName(),
                        UMLPackage.eINSTANCE.getActivity().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseActivityPartition(ActivityPartition activityPartition) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Activity
                    || this.newSemanticContainer instanceof ActivityPartition) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, activityPartition.eClass().getName(),
                        UMLPackage.eINSTANCE.getActivity().getName() + COMA
                                + UMLPackage.eINSTANCE.getActivityPartition().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof ActivityGroup || this.newSemanticContainer instanceof Activity) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, comment.eClass().getName(),
                        UMLPackage.eINSTANCE.getActivity().getName() + COMA
                                + UMLPackage.eINSTANCE.getActivityGroup().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredActivityNode
                    || this.newSemanticContainer instanceof Activity) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, constraint.eClass().getName(),
                        UMLPackage.eINSTANCE.getActivity().getName() + COMA
                                + UMLPackage.eINSTANCE.getStructuredActivityNode().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseElement(Element element) {
            return CheckStatus.no(UNAUTHORIZED_DND_ERROR_MSG);
        }

        @Override
        public CheckStatus caseExpansionNode(ExpansionNode expansionNode) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof ExpansionRegion) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, expansionNode.eClass().getName(),
                        UMLPackage.eINSTANCE.getExpansionRegion().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseInterruptibleActivityRegion(InterruptibleActivityRegion interruptibleActivityRegion) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Activity) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus
                        .no(MessageFormat.format(DROP_ERROR_MSG, interruptibleActivityRegion.eClass().getName()));
            }
            return result;
        }
    }
}
