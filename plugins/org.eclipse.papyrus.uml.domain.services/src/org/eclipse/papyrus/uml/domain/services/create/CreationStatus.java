/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.State;

/**
 * Status after creation request.
 *
 * <p>
 * A {@link State#DONE} does not necessary means a new elements has been
 * created. It means that the process ended normally
 * </p>
 *
 * @author Arthur Daussy
 */
public class CreationStatus {

    public static final CreationStatus NOTHING = new CreationStatus(State.NOTHING, null, null);

    private final State state;

    private final String message;

    private final EObject element;

    public CreationStatus(State state, String message, EObject element) {
        super();
        this.state = state;
        this.message = message;
        this.element = element;
    }

    public static CreationStatus createFailingStatus(String message) {
        return new CreationStatus(State.FAILED, message, null);
    }

    public static CreationStatus createOKStatus(EObject eObject) {
        return new CreationStatus(State.DONE, null, eObject);
    }

    public State getState() {
        return this.state;
    }

    public String getMessage() {
        return this.message;
    }

    /**
     * Returns the created element if any.
     *
     * @return an {@link EObject} or null
     */
    public EObject getElement() {
        return this.element;
    }

}
