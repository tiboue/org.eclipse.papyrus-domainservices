/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;

/**
 * Provider of behavior when dropping semantic elements (from Explorer) to a
 * diagram element.
 * 
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public interface IExternalSourceToRepresentationDropBehaviorProvider {
    /**
     * Drops a semantic element on another.
     * 
     * @param droppedElement
     *                        the element being dropped
     * @param target
     *                        the semantic target of the drop
     * @param crossRef
     *                        a {@link ECrossReferenceAdapter}
     * @param editableChecker
     *                        a {@link IEditableChecker}
     * @return a {@link DnDStatus} of the drop, i.e. status of the drop and a set of
     *         elements to display or the elements failed to display.
     */
    DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker);

}
