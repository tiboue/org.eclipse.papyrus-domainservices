/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.State;

/**
 * Status after a semantic DnD request.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public final class DnDStatus {

    private final State state;

    private final String message;

    private final Set<EObject> elementsToDisplay;

    private DnDStatus(State state, String message, Set<EObject> elementsToDisplay) {
        super();
        this.state = state;
        this.message = message;
        this.elementsToDisplay = elementsToDisplay;
    }

    public static DnDStatus createFailingStatus(String message, Set<EObject> elements) {
        return new DnDStatus(State.FAILED, message, elements);
    }

    public static DnDStatus createOKStatus(Set<EObject> elements) {
        return new DnDStatus(State.DONE, "", elements);
    }

    public static DnDStatus createNothingStatus(Set<EObject> elements) {
        return new DnDStatus(State.NOTHING, "", elements);
    }

    public State getState() {
        return this.state;
    }

    public String getMessage() {
        return this.message;
    }

    /**
     * Returns the full list of elements to display on the diagram after semantic
     * DnD.
     *
     * @return a Set of {@link EObject} or null
     */
    public Set<EObject> getElementsToDisplay() {
        return this.elementsToDisplay;
    }

}
